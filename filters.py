'''
ToDo:
1. Build Haar Cascade
2. Test Cascade with Static Image
    - Retrain cascade if necessary
3. Test Cascade with Video Capture
4. Implement RoboRIO communication capability
'''
import cv2
import numpy as np
import socket

class Track():
    def __init__(self, camInt, cascadePath):
        self.cap = cv2.VideoCapture(camInt)
        self.cascade = cv2.CascadeClassifier(cascadePath)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    def frames(self):
       	_, self.frame = self.cap.read()
       	self.frame = cv2.resize(self.frame, (500,500))
       	gray = cv2.cvtColor(self.frame, cv2.COLOR_BGR2GRAY)
       	self.targets = self.cascade.detectMultiScale(gray, 1.03, 7)
    def analyze(self):
        maxArea = 0
        a = 0
        b = 0
        c = 0
        d = 0
        for (x,y,w,h) in self.targets:
            if (w*h != 0 and w*h > maxArea):
                a = x
                b = y
                c = w
                d = h
                maxArea = w*h
        if(maxArea > 5000):
            self.frame = cv2.rectangle(self.frame, (a,b), (a+c,b+d), (255,255,255), 2)
            ctr = (a+(c/2),b+(d/2))
       	cv2.imshow('Frame', self.frame)
       	return ctr
    def targetShift(self, target):
        return 0
    def sendData(self, val, targetIP, port):
        self.sock.sendto(val, (targetIP, port))
    def terminate(self):
        self.cap.release()
        cv2.destroyAllWindows()