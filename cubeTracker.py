import cv2
import numpy as np
from filters import Track

CASCADE_PATH = 'cascadeTraining/lbp/cascade.xml'
UDP_IP = ''
UDP_PORT = ''

vis = Track(2, CASCADE_PATH)
while(True):
    try:
        vis.frames()
        centerPoint = vis.analyze()
        print("Center of Target at " + str(centerPoint))
        #vis.sendData(centerPoint, UDP_IP, UDP_PORT)
        key = cv2.waitKey(1)
        if(key == ord('q')):
            break
    except Exception as e:
        print(str(e))
vis.terminate()
