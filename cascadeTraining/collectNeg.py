import cv2
import numpy as np
import urllib.request
import os

def webScrape(link, index):
    urls = urllib.request.urlopen(link).read().decode()
    for i in urls.split('\n'):
        try:
            print(i)
            urllib.request.urlretrieve(i, 'neg/' + str(index) + '.jpg')
            img = cv2.imread('neg/' + str(index) + '.jpg', cv2.IMREAD_GRAYSCALE)
            resized_img = cv2.resize(img, (150, 150))
            cv2.imwrite('neg/' + str(index) + '.jpg', resized_img)
            index += 1
        except Exception as e:
            print(str(e))
    return index
def killNeg(ref):
    for fileType in ['neg']:
        for img in os.listdir(fileType):
            for error in os.listdir('error'):
                try:
                    imgPath = str(fileType) + '/' + str(img)
                    ref = cv2.imread('error/' + str(error))
                    candidate = cv2.imread(imgPath)
                    if (candidate.shape == ref.shape and not(cv2.bitwise_xor(ref, candidate).any())):
                        print("Bad Image detected")
                        print(imgPath)
                        os.remove(imgPath)
                except Exception as e:
                    print(str(e))
def resize():
    for fileType in ['neg']:
        for imgName in os.listdir(fileType):
            img = cv2.imread('neg/' + imgName)
            img = cv2.resize(img, (100,100))
            cv2.imwrite('neg' + '/' + str(imgName), img)

LINK_1 = 'http://image-net.org/api/text/imagenet.synset.geturls?wnid=n02761392'
LINK_2 = 'http://image-net.org/api/text/imagenet.synset.geturls?wnid=n02472987'

#a = webScrape(LINK_1, 1)
#b = webScrape(LINK_2, a + 1)
#killNeg('error/error.jpg')
#resize()